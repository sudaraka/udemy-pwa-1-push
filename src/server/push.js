// push module

const
  webpush = require('web-push'),
  urlsafeBase64 = require('urlsafe-base64'),
  Storage = require('node-storage'),
  vapid = require('./vapid'),

  // Subscription store
  store = new Storage(`${__dirname}/db`),
  subscriptions = store.get('subscriptions') || []


webpush.setVapidDetails(
  'mailto:example@example.com',
  vapid.publicKey,
  vapid.privateKey
)

// Create URL safe VAPID public key
module.exports.getKey = () => urlsafeBase64.decode(vapid.publicKey)

// Store new subscription
module.exports.addSubscription = subscription => {
  // Add to subscription store
  subscriptions.push(subscription)

  // Persist subscriptions
  store.put('subscriptions', subscriptions)
}

// Send notification to all registered subscriptions
module.exports.send = msg => {
  // Queue notification promises for processing after all of them resolved
  const
    sentNotifications = []

  subscriptions.forEach((subscription, i) => {
    const
      // Send notification
      notificationPromise = webpush.sendNotification(subscription, msg)
        .catch(status => {
          // Check for HTTP 410 (Gone) response
          if(410 == status.statusCode) {
            subscriptions[i].delete = true
          }

          // Return a value without rejecting the promise
          // NOTE: This is required for the `Promise.all` that is used in the
          // clean up operation below.
          return null
        })

    sentNotifications.push(notificationPromise)
  })

  // Clean subscriptions marked for deletion
  Promise.all(sentNotifications)
    .then(() => {
      const
        validSubscriptions = subscriptions
          // Only keep subscription that are NOT marked to be deleted
          .filter(n => !n.delete)

      // Persist only valid subscriptions.
      store.put('subscriptions', validSubscriptions)
    })
}
