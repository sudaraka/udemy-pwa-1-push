// Server main module

const
  http = require('http'),
  push = require('./push')


http
  // Create HTTP server
  .createServer((req, res) => {
    const
      {url, method} = req

    // Enable CORS
    res.setHeader('Access-Control-Allow-Origin', '*')

    // Subscribe
    if('POST' === method && url.match(/^\/subscribe\/?/)) {
      const
        body = []

      req
        .on('data', chunk => body.push(chunk))
        .on('end', () => {
          const
            // Parse subscription body to object
            subscription = JSON.parse(body.toString())

          // Store subscription for push notification
          push.addSubscription(subscription)

          res.end('Subscribed')
        })
    }

    // Public Key
    else if('GET' === method && url.match(/^\/key\/?/)) {
      res.end(push.getKey())
    }

    // Push Notification
    else if('POST' === method && url.match(/^\/push\/?/)) {
      const
        body = []

      req
        .on('data', chunk => body.push(chunk))
        .on('end', () => {
          // Send notification with POST body
          push.send(body.toString())

          res.end('Push sent\n')
        })
    }

    // Fallback Route (404)
    else {
      res.status = 404
      res.end('Error: Unknown request')
    }
  })
  // Start listening
  .listen(5001, () => console.log('Server Running.'))

